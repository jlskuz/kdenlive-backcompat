# Kdenlive compatibility test suite

Currently, we do not test for or guarantee backwards compatibility. This means
that it is unsafe for a user to open a Kdenlive project created in an older
version of Kdenlive. Possible issues include:

* Not being able to open the file at all
* Effects or compositions deleted (should see warning)
* Timeline corruption
* Differences in the rendered result

(Because it's unsafe to open older projects in newer version of Kdenlive, I
think using the Ubuntu PPA is a little dangerous because the OS updater will
update Kdenlive, sometimes without the user noticing.)

The purpose of this repo is to start a collection of Kdenlive projects that
exercise different Kdenlive features. Each version of Kdenlive should render
the same result for each project - otherwise, we've found an incompatibility.

## Files

* test-cases.yaml - a list of Kdenlive projects in this repo, along with some
  metadata
* projects/ - sample video projects that may refer to files in `footage/`.
* projects/footage/ - sample video files that Kdenlive should be able to handle. Because
  video files tend to take up a lot of space, we should try to reuse the same
  files or provide small sample files.
* results/ - an empty directory used for storing render results. Render results
  will not be checked into git.

## Rendering test projects

1. Some of the test projects use "DejaVu Sans Mono". If you do not have this
font, download it from
https://dejavu-fonts.github.io/Download.html and install it.
2. Download the Kdenlive version you are using and install or extract it if
necessary.
3. Open up each project and fix any missing media. For luma PGM files (e.g.
linear_x.pgm), you may need to point Kdenlive to the matching file in your
Kdenlive install. Make sure to use the HD version and not the PAL version.
4. Render the project without making any other changes. By convention, the
rendered output goes in `results/\[kdenlive-build\]/\[project-name\].mp4`.
For example,
`results/kdenlive-22.08.2.dmg/composite-and-transform-21.12.3.mp4`. This make it
easier to know which version the project was rendered in. Videos rendered in the
original version used to create the project are put in `results/reference/`.
These videos can be downloaded from
https://files.kde.org/kdenlive/kdenlive-backcompat/results/reference/ if you
want to compare your rendered version with the original.

## Checking for video differences

### Setup

These commands should be run in the directory where you checked out this repo,
e.g. `cd ~/kdenlive-backcompat` first.

To install the required Python dependencies using Pipenv, first setup a Pipenv
virtual environment:

`pipenv --python 3`

Then install the necessary Python packages:

`pipenv install`

### Comparing files

The script `compare.py` takes two video files and compares them frame-by-frame,
outputting how many frames are different between each file. (It cannot account
for frames shifted earlier or later.)

Basic usage:

`pipenv run python compare.py video1.mp4 video2.mp4`

(You do not need to prepend `pipenv run` if already in the virtual environment
or another suitable environment.)

Additional flags are described by running:

`python compare.py --help`

One thing that may be useful in manual testing is having it stop on the first
frame, and also save the two different versions of the first frame to disk:

`python compare.py --stop-when-different --save-difference video1.mp4 video2.mp4`

Stacking the two images in a program such as Krita can help visualize the areas
in the frame that are different.

### Comparing multiple files

For more conveniently testing multiple files against the reference renders, you
can give `compare_dir.py` a directory containing multiple mp4 files. The naming
convention for the videos is:

`results/[build_name]/[test_name].mp4`

So you would run `python compare_dir.py results/[build_name]`.

For example:

`python compare_dir.py results/kdenlive-22.08.2-x86_x64.AppImage/`

Then the script will compare each MP4 in that directory against the MP4 with the
same name in `results/reference/`. At the end, it will print out a list of
results in YAML format suitable for adding to `test-results.yaml`.
