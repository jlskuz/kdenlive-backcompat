- name: Ajax-loader.gif
  source: https://commons.wikimedia.org/wiki/File:Ajax-loader.gif
  license: public domain
- name: checkerboard.png
  source: own work
  license: with repo
- name: Dolphin_pantropical.jpg
  source: https://commons.wikimedia.org/wiki/File:Dolphin_pantropical.jpg
  license: public domain
- name: kdenlive-logo.png
  source: kdenlive
  license: GPLv3
- name: kdenlive-logo.gif
  source: converted from kdenlive-logo.png
  license: GPLv3
- name: kdenlive-logo.tga
  source: converted from kdenlive-logo.png
  license: GPLv3
- name: webp-lossless.webp
  source: https://developers.google.com/speed/webp/gallery2
  license: CC BY-SA 3.0
- name: webp-lossy.webp
  source: https://developers.google.com/speed/webp/gallery2
  license: CC BY-SA 3.0
- name: Kdenlive-logo.svg
  source: https://commons.wikimedia.org/wiki/File:Kdenlive-logo.svg
  license: CC0 1.0
- name: chris-charles-9APFPoNb9iw-unsplash.jpg
  source: https://unsplash.com/photos/9APFPoNb9iw
  license: Unsplash License
