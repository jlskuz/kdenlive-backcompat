import yaml

def main():
    with open('test-cases.yaml', 'r') as f:
        test_cases = yaml.safe_load(f)
    with open('test-results.yaml', 'r') as f:
        test_results = yaml.safe_load(f)

    with open('template-results-table.html', 'r') as f:
        print(f.read())

    for ver in test_results['tested_versions']:
        print("    <th class=\"version {1}\">{0}</th>".format(ver, "os-" + version_os(ver)))
    print("</tr>")

    test_cases.sort(key=lambda c: c['name'])

    for details in test_cases:
        print("<tr>")
        print("    <td>{}</td>".format(details['name']))
        for ver in test_results['tested_versions']:
            for result in test_results['tested_projects']:
                if result['project'] == details['name'] and result['version'] == ver:
                    percentage_diff = (1-result['different_frames']/result['actual_frames'])*100
                    if percentage_diff == 100 and result['expected_frames'] == result['actual_frames']:
                        result_class = "good"
                    else:
                        result_class = "bad"

                    result_class += " os-" + version_os(result['version'])

                    print("    <td class=\"result {}\">{:.1f}%".format(result_class, percentage_diff))
                    if result['expected_frames'] != result['actual_frames']:
                        print("<br />⚠️ Expected {} frames, got {}".format(result['expected_frames'], result['actual_frames']))
                    if 'bugs' in result and result['bugs']:
                        print("<br />🪲 Bugs: {}".format(", ".join([
                            "<a href=\"https://invent.kde.org/multimedia/kdenlive/-/issues/{0}\">#{0}</a>".format(bid) for bid in result['bugs']
                        ])))
                    break
            else:
                print("<td class=\"result no-result {0}\">?".format("os-" + version_os(ver)))
            print("</td>")
        print("</tr>")

    print("""
</table>
</body>
</html>
    """)

def version_os(version_str):
    if ".exe" in version_str:
        return "win"
    elif ".AppImage" in version_str:
        return "linux"
    elif ".dmg" in version_str:
        return "mac"
    else:
        return "unknown"

if __name__ == "__main__":
    main()
